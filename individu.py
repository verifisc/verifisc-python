# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from z3 import *
from montant import *
from foyerfiscal import *
from typing import Any, Optional
from tabulate import tabulate


class Individu:
    def __init__(
        self,
        id: BitVec,
        salaires: List[Montant],
        en_activite: Bool,
        invalide: Bool,
        age: BitVec,
        scolarise: Bool,
        is_adulte: bool
    ):
        if len(salaires) != 12:
            raise TypeError("number of salaires must be 12")
        self.salaires = salaires
        self.en_activite = en_activite
        self.age = age
        self.invalide = invalide
        self.scolarise = scolarise
        self.is_adulte = is_adulte
        self.id = id

        self.revenu_annuel_: Optional[Montant] = None

    def __eq__(self, rhs: Any) -> Any:
        return And(
            self.id == rhs.id,
            And([self.salaires[i] == rhs.salaires[i] for i in range(12)]),
            self.en_activite == rhs.en_activite,
            self.age == rhs.age,
            self.invalide == rhs.invalide,
            self.scolarise == rhs.scolarise,
            self.is_adulte == rhs.is_adulte
        )

    def __neq__(self, rhs: Any) -> Any:
        return Or(
            self.id != rhs.id,
            [self.salaires[i] != rhs.salaires[i] for i in range(12)],
            self.en_activite != rhs.en_activite,
            self.age != rhs.age,
            self.invalide != rhs.invalide,
            self.scolarise != rhs.scolarise,
            self.is_adulte != rhs.is_adulte
        )

    @staticmethod
    def variable(name: str, s: Solver, is_adulte: bool) -> 'Individu':
        salaires = [
            Montant.anonyme(s, name + "_salaire_{}".format(i))
            for i in range(0, 12)
        ]
        en_activite = Bool(name + "_en_activite")
        invalide = Bool(name + "_invalide")
        scolarise = Bool(name + "_scolarise")
        for i in range(0, 12):
            # HYPOTHÈSE: impossible de gagner plus de 100 000 € par mois
            s.add(salaires[i] <= Montant.euros(100000, s))
            # HYPOTHÈSE: impossible de gagner moins de 783,12 € par mois en activité
            s.add(Or(salaires[i] == Montant.euros(0, s), salaires[i] >= Montant.smic_mi_temps_net(s)))
            s.add(
                Implies(en_activite,
                    salaires[i] > Montant.euros(0, s)
                ),
                Implies(Not(en_activite),
                    salaires[i] == Montant.euros(0, s)
                )
            )
        age = BitVec(name + "_age", repr_bits)
        # HYPOTHÈSE: l'âge de tout individu est inférieur à 120 ans
        s.add(age >= 0, age <= 120)
        # HYPOTHÈSE: un enfant de moins de 16 ans ne peut être en activité
        s.add(Not(And(en_activite, age < 16)))
        # HYPOTHÈSE: un enfant dont l'âge est entre 3 et 16 ans est obligatoirement scolarisé
        s.add(Not(And(age <= 16, age >= 3, Not(scolarise))))
        id = BitVec(name + "_id", repr_bits)
        return Individu(id, salaires, en_activite, invalide, age, scolarise, is_adulte)

    def same_individu_but_different_revenue(self, name: str, s: Solver) -> "Individu":
        out = Individu.variable(name, s, self.is_adulte)
        s.add(self.id == out.id)
        s.add(self.en_activite == out.en_activite)
        s.add(self.invalide == out.invalide)
        s.add(self.scolarise == out.scolarise)
        return out

    def evaluate(self, s: Solver) -> 'Individu':
        return Individu(
            self.id,
            [self.salaires[i].evaluate(s) for i in range(0, 12)],
            s.model().evaluate(self.en_activite),
            s.model().evaluate(self.age).as_long(),
            s.model().evaluate(self.invalide),
            s.model().evaluate(self.scolarise),
            self.is_adulte
        )

    def revenu_annuel(self, s: Solver) -> Montant:
        if self.revenu_annuel_ is None:
            sum = Montant.euros(0, s)
            for i in range(0, 12):
                sum += self.salaires[i]
            self.revenu_annuel_ = sum.round_to_euro()
        return self.revenu_annuel_

    def salaires_3_derniers_mois(self) -> Montant:
        return self.salaires[11] + self.salaires[10] + self.salaires[9]

    def salaire_constant(self, s: Solver) -> None:
        for i in range(1, 12):
            s.add(self.salaires[i] == self.salaires[0])

    def augmentation_dernier_salaire(self, m: Montant) -> 'Individu':
        new_salaires = [
            self.salaires[i] if i != 11 else self.salaires[i] + m
            for i in range(0, 12)
        ]
        return Individu(
            self.id,
            new_salaires,
            self.en_activite,
            self.invalide,
            self.age,
            self.scolarise,
            self.is_adulte
        )

    def augmentation_salaire(self, m: Montant) -> 'Individu':
        new_salaires = [
            self.salaires[i] + m
            for i in range(0, 12)
        ]
        return Individu(
            self.id,
            new_salaires,
            self.en_activite,
            self.invalide,
            self.age,
            self.scolarise,
            self.is_adulte
        )

    @staticmethod
    def list_to_str(l: List['Individu']) -> str:
        if len(l) == 0:
            return "Pas de personnes à charge !"

        lines = [
            ["En activité"] + [indiv.en_activite for indiv in l],  # type: ignore
        ]

        if not l[0].is_adulte:
            lines.extend([
                ["Scolarisé"] + [indiv.scolarise for indiv in l],  # type: ignore
                ["Âge"] + [indiv.invalide for indiv in l],  # type: ignore
                ["Invalide"] + [indiv.age for indiv in l]  # type: ignore
            ])
        if all([
            not indiv.en_activite
            for indiv in l
        ]):
            lines = lines
            # Nothing
        elif all([
            all([indiv.salaires[i] == indiv.salaires[0] for i in range(0, 12)])
            for indiv in l
        ]):
            lines.extend([
                ["Salaire mensuel"] + [indiv.salaires[0] for indiv in l],  # type: ignore
            ])
        else:
            lines.extend([
                ["Salaire janvier"] + [indiv.salaires[0] for indiv in l],  # type: ignore
                ["Salaire février"] + [indiv.salaires[1] for indiv in l],  # type: ignore
                ["Salaire mars"] + [indiv.salaires[2] for indiv in l],  # type: ignore
                ["Salaire avril"] + [indiv.salaires[3] for indiv in l],  # type: ignore
                ["Salaire mai"] + [indiv.salaires[4] for indiv in l],  # type: ignore
                ["Salaire juin"] + [indiv.salaires[5] for indiv in l],  # type: ignore
                ["Salaire juillet"] + [indiv.salaires[6] for indiv in l],  # type: ignore
                ["Salaire août"] + [indiv.salaires[7] for indiv in l],  # type: ignore
                ["Salaire septembre"] + [indiv.salaires[8] for indiv in l],  # type: ignore
                ["Salaire octobre"] + [indiv.salaires[9] for indiv in l],  # type: ignore
                ["Salaire novembre"] + [indiv.salaires[10] for indiv in l],  # type: ignore
                ["Salaire décembre"] + [indiv.salaires[11] for indiv in l],  # type: ignore
            ])
        table = tabulate(
            lines,
            ["Caractéristique"] + ["Valeur pour l'individu {}".format(i + 1) for i in range(0, len(l))],  # type: ignore
            tablefmt="pipe"
        )
        return str(table)
