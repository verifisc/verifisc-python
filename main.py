# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from questions import *
from query import dichotomic_search
from os import mkdir
from typing import Any
import signal
import sys


# We exit the program with Ctrl+C
def signal_handler(sig: Any, frame: Any) -> None:
        print('You pressed Ctrl+C, exiting the program')
        sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)


# Preventively creates the results directory
try:
    mkdir("results")
except FileExistsError:
    # Nothing happens
    ()

full_features = set(["IR", "AF", "ARS", "BC", "BL", "APL", "PA"])

menage_constraints = MenageConstraints(
    max_invalides=0,
    max_personnes_a_charge=3,
    min_salaires_total_mensuel=1500,
    max_salaires_total_mensuel=3000,
    min_ratio_loyer_salaire=20,
    max_ratio_loyer_salaire=40,
    min_base_rent=500,
    min_per_person_rent_increment=200,
    zone_3_percent_multiplier=60,
    zone_2_percent_multiplier=80,
    zone_1_percent_multiplier=100,
    avoid_concubinage=False,
    avoid_couples=False,
    avoid_hebergement_gratuit=True,
)

question_new_child = DifferenceNouvelEnfant(
    menage_constraints,
    relative_true_absolue_false=True,
    max_true_min_false=False,
    initial_search_param_value=5,
    lower_bound=-100,
    upper_bound=20,
    precision=1,
    changement_logement=True,
    nb_personnes_a_charge_init=0,
    age_nouvel_enfant=None,
)

question_free_evolution = DifferenceAbsolueEvolutionLibre(
    menage_constraints,
    max_variation_revenus=200,
    max_variation_personnes_charge=1,
    max_variation_loyer=100,
    max_true_min_false=False,
    initial_search_param_value=-400,
    lower_bound=-600,
    upper_bound=-200,
    precision=30,
)

question_marginal_tax_rate = TauxMarginalMaximalApresAugmentation(
    menage_constraints,
    augmentation_mensuelle=250,
    initial_search_param_value=70,
    lower_bound=0,
    upper_bound=200,
    precision=2
)

question_2019_2020 = Difference2019IR2020(
    menage_constraints,
    relative_true_absolue_false=True,
    max_true_min_false=False,
    initial_search_param_value=100,
    lower_bound=-100,
    upper_bound=200,
    precision=1,
)

dichotomic_search(
    question=question_2019_2020,
    send_to_telegram=False,
    features=set(["2019IR2020"])
)
