# Verifisc python prototype

## Installation

Create a Python3.7 Python virtual environnement with :

    virtualenv -p python3.7 env

Then activate it:

    source env/bin/activate

Install the requirements :

    pip install -r requirements.txt

You can test using

    pytest

In order to send Telegram notifications once you query is complete, you need a `TelegramBotToken.txt` file in this directory containing the Telegram API Bot token, and a `TelegramChatID.txt` file in this directory containing the ID of the Telegram channel you want to send the messages to.


## Usage

This software encodes a number of French socio-fiscal measures such as income tax, family benefits, etc. into a format understandable by the Z3 SMT solver. The correctness of this implementation of the legislation is not guaranteed, however it has been tested by comparing the outputs of the program to official French tax simulators. This implementation supposes a number of restrictive hypotheses on the space of possible households and situations, which you can see using `make hypotheses`.

This prototype also offers an interface to query the model and gain insights on the French socio-fiscal system. This interface comes under the form of a dichotomic search aimed at answering to a question under the form :

    What is the household that maximises/minimizes the value X ?

Where `X` can be defined very broadly, but usually as the relative or absolute difference of net income after redistribution of the same household, before and after some change. The way to interrogate the model is defined as an abstract class in `questions.py`, which you can instantiate with a new question tackling a different situation if you'd like.

You can modify you query in `main.py` and then launch `make run`.

The results found are stored in the `results` folder.

## Features

You can select which features to include in your analysis before making your query. Here is a list of the currently supported features :
* `IR`: income tax (*Impôt sur le Revenu*);
* `AF`: family benefits (*Allocations Familiales*);
* `ARS`: back-to-school benefit (*Allocation de Rentrée Scolaire*);
* `BC`: middle-school grant (*Bourse Collège*);
* `BL`: highschool grant (*Bourse lycée*);
* `APL`: rent subsidy (*Allocation Personnalisée au Logement*);
* `PA`: low-wage subsidy (*Prime d'Activité*).

The fiscal households supported by the model have many different parameters that can vary :
* matrimonial situation (single, married or concubines);
* number of dependent persons, each of them with his/her age and school situation;
* professional revenues (salaries only) for each adult of the household;
* free housing or rented housing, with a rent depending on the revenue of the household and the geographical zone.

## Limitations

### Bitvectors

Because of the *Reduction d'impôt sous conditions de revenus* provision in the computation of the income tax, the computation is not linear. That forces us, in order to have reliable results from Z3, to use the BitVector theory. We use 34-bits words to encode all amounts of money involved in the computation, so those cannot exceed 20 million euros. This restricts the maximum yearly revenue of the households to ~ 200 k € per year, but that should be enough to cover most cases.

### Runtime and RAM

The French socio-fiscal model is complicated, and this prototype produces very big and quite non-optimized queries. For a query that is easily SAT or UNSAT, you can count on anywhere between 30 minutes and a few hours of runtime, and a RAM usage of up to 5 Go. But for difficult queries that lie on the boundary between SAT and UNSAT, a single query can take up to 48 hours and consume more than 120 Go of RAM.

Optimizing the queries sent to Z3 is a plan of improvement, but in the meantime you can use the Telegram notification system to monitor your queries.
