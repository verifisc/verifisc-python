# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>


from montant import *
from foyerfiscal import *
from IR2019 import *
from IR2020 import *
from solveur import *
from individu import *
from menage import *
from z3 import *
from prime_activite2019 import *
from allocations_familiales2019 import *
from APL2019 import *
from typing import Set, Optional, List, Any, Tuple, Union
from abc import ABC, abstractmethod


class MenageConstraints:
    def __init__(
        self,
        max_invalides: int,
        max_personnes_a_charge: int,
        min_salaires_total_mensuel: int,
        max_salaires_total_mensuel: int,
        min_ratio_loyer_salaire: int,
        max_ratio_loyer_salaire: int,
        min_base_rent: int,
        min_per_person_rent_increment: int,
        zone_3_percent_multiplier: int,
        zone_2_percent_multiplier: int,
        zone_1_percent_multiplier: int,
        avoid_hebergement_gratuit: bool,
        avoid_couples: bool,
        avoid_concubinage: bool
    ):
        self.max_invalides = max_invalides
        self.max_personnes_a_charge = max_personnes_a_charge
        self.min_salaires_total_mensuel = min_salaires_total_mensuel
        self.max_salaires_total_mensuel = max_salaires_total_mensuel
        self.min_ratio_loyer_salaire = min_ratio_loyer_salaire
        self.max_ratio_loyer_salaire = max_ratio_loyer_salaire
        self.min_base_rent = min_base_rent
        self.min_per_person_rent_increment = min_per_person_rent_increment
        self.zone_3_percent_multiplier = zone_3_percent_multiplier
        self.zone_2_percent_multiplier = zone_2_percent_multiplier
        self.zone_1_percent_multiplier = zone_1_percent_multiplier
        self.avoid_concubinage = avoid_concubinage
        self.avoid_couples = avoid_couples
        self.avoid_hebergement_gratuit = avoid_hebergement_gratuit

    def constrain_menage(self, m: Menage, s: Solver) -> None:
        invalides = m.nb_invalides_a_charge(s)
        npc = m.nb_personnes_a_charge(s)
        m.salaire_constant(s)
        max_ratio_loyer_salaire_t = Taux.pourcent(self.max_ratio_loyer_salaire)
        min_ratio_loyer_salaire_t = Taux.pourcent(self.min_ratio_loyer_salaire)
        m.ratio_loyer_salaire(min_ratio_loyer_salaire_t, max_ratio_loyer_salaire_t, s)
        m.mensualite_minimale(
            self.min_base_rent,
            self.min_per_person_rent_increment,
            self.zone_3_percent_multiplier,
            self.zone_2_percent_multiplier,
            self.zone_1_percent_multiplier,
            s
        )
        max_salaires_total_mensuel_m = Montant.euros(self.max_salaires_total_mensuel, s)
        min_salaires_total_mensuel_m = Montant.euros(self.min_salaires_total_mensuel, s)
        s1 = m.revenu_salaires(s)
        s.add(
            s1 <= max_salaires_total_mensuel_m * 12,
            min_salaires_total_mensuel_m * 12 <= s1,
            invalides <= self.max_invalides,
            npc <= self.max_personnes_a_charge,
        )
        if self.avoid_concubinage:
            s.add(m.composition != Composition.concubinage())
        if self.avoid_couples:
            s.add(m.composition != Composition.couple())
        if self.avoid_hebergement_gratuit:
            s.add(Not(m.logement.is_herbergement_gratuit()))

    def table_params_lines(self, s: Solver) -> List[List[Any]]:
        return [
            ["Nombre maximal d'invalides à charge", self.max_invalides],
            ["Nombre maximal de personnes à charge", self.max_personnes_a_charge],
            ["Revenus mensuel salarial minimal", Montant(self.min_salaires_total_mensuel * 100, s)],
            ["Revenus mensuel salarial maximal", Montant(self.max_salaires_total_mensuel * 100, s)],
            ["Ratio loyer/salaire minimal", "{}%".format(self.min_ratio_loyer_salaire)],
            ["Ratio loyer/salaire maximal", "{}%".format(self.max_ratio_loyer_salaire)],
            ["Base du loyer minimal", Montant(self.min_base_rent * 100, s)],
            ["Supplément par personne du loyer minimal", Montant(self.min_per_person_rent_increment * 100, s)],
            ["Multiplicateur zone III", "{}%".format(self.zone_3_percent_multiplier)],
            ["Multiplicateur zone II", "{}%".format(self.zone_2_percent_multiplier)],
            ["Multiplicateur zone I", "{}%".format(self.zone_1_percent_multiplier)],
            ["Éviter le concubinage", self.avoid_concubinage],
            ["Éviter les couples", self.avoid_couples],
            ["Éviter l'hébergement gratuit", self.avoid_hebergement_gratuit],
        ]


class MenageTransform(ABC):
    @abstractmethod
    def __init__(self) -> None:
        self.m1: Optional[Menage] = None
        self.m2: Optional[Menage] = None

    @abstractmethod
    def table_params_lines(self, s: Solver) -> List[List[Any]]:
        pass

    @abstractmethod
    def transform_menage(self, m1: Menage, s: Solver) -> Menage:
        pass

    def menage_avant(self) -> Menage:
        if self.m1 is None:
            raise TypeError("menage avant is none!")
        else:
            return self.m1

    def menage_apres(self) -> Menage:
        if self.m2 is None:
            raise TypeError("menage avant is none!")
        else:
            return self.m2


class Augmentation(MenageTransform):

    def __init__(
        self,
        augmentation_mensuelle: int,
    ):
        self.augmentation_mensuelle = augmentation_mensuelle
        self.m1: Optional[Menage] = None
        self.m2: Optional[Menage] = None

    def table_params_lines(self, s: Solver) -> List[List[Any]]:
        return [
            ["Augmentation mensuelle du premier individu", self.augmentation_mensuelle],
        ]

    def transform_menage(self, m1: Menage, s: Solver) -> Menage:
        self.m1 = m1
        s.add(m1.f1.individus[0].en_activite)
        augmentation_mensuelle_m = Montant.euros(self.augmentation_mensuelle, s)
        m2 = m1.augmentation_salaire_premier_individu(augmentation_mensuelle_m)
        self.m2 = m2
        return m2


class Identite(MenageTransform):

    def __init__(
        self,
    ) -> None:
        self.m1: Optional[Menage] = None
        self.m2: Optional[Menage] = None

    def table_params_lines(self, s: Solver) -> List[List[Any]]:
        return []

    def transform_menage(self, m1: Menage, s: Solver) -> Menage:
        self.m1 = m1
        m2 = m1
        self.m2 = m2
        return m2


class MarriageOuPacs(MenageTransform):
    def __init__(self) -> None:
        self.m1: Optional[Menage] = None
        self.m2: Optional[Menage] = None

    def table_params_lines(self, s: Solver) -> List[List[Any]]:
        return []

    def transform_menage(self, m1: Menage, s: Solver) -> Menage:
        self.m1 = m1
        m2 = m1.mariage_ou_pacs(s)
        self.m2 = m2
        return m2


class NouvelEnfant(MenageTransform):
    def __init__(self,
        menage_constraints: MenageConstraints,
        changement_logement: bool=False,
        nb_personnes_a_charge_init: Optional[int]=None,
        age_nouvel_enfant: Optional[int]=None,
    ):
        self.menage_constraints_ = menage_constraints
        self.changement_logement = changement_logement
        self.nb_personnes_a_charge_init = nb_personnes_a_charge_init
        self.age_nouvel_enfant = age_nouvel_enfant

        self.m1: Optional[Menage] = None
        self.m2: Optional[Menage] = None

    def table_params_lines(self, s: Solver) -> List[List[Any]]:
        return [
            ["Changement de logement avec le nouvel enfant", self.changement_logement],
            ["Nombre initial de personnes à charge", self.nb_personnes_a_charge_init],
            ["Âge du nouvel enfant", self.age_nouvel_enfant]
        ]

    def transform_menage(self, m1: Menage, s: Solver) -> Menage:
        self.m1 = m1
        if self.nb_personnes_a_charge_init is not None:
            s.add(m1.nb_personnes_a_charge(s) == self.nb_personnes_a_charge_init)
        m2 = m1.nouvel_enfant(s, self.age_nouvel_enfant, self.changement_logement)
        m2.mensualite_minimale(
            self.menage_constraints_.min_base_rent,
            self.menage_constraints_.min_per_person_rent_increment,
            self.menage_constraints_.zone_3_percent_multiplier,
            self.menage_constraints_.zone_2_percent_multiplier,
            self.menage_constraints_.zone_1_percent_multiplier,
            s
        )
        max_ratio_loyer_salaire_t = Taux.pourcent(self.menage_constraints_.max_ratio_loyer_salaire)
        min_ratio_loyer_salaire_t = Taux.pourcent(self.menage_constraints_.min_ratio_loyer_salaire)
        m2.ratio_loyer_salaire(min_ratio_loyer_salaire_t, max_ratio_loyer_salaire_t, s)
        self.m2 = m2
        return m2


class ChangementDistributionRevenus(MenageTransform):
    def __init__(self) -> None:
        self.m1: Optional[Menage] = None
        self.m2: Optional[Menage] = None

    def table_params_lines(self, s: Solver) -> List[List[Any]]:
        return []

    def transform_menage(self, m1: Menage, s: Solver) -> Menage:
        s.add(Or(
            m1.composition == Composition.concubinage(),
            m1.composition == Composition.couple()
        ))
        self.m1 = m1
        self.m2 = m1.nouvelle_repartition_revenus(s)
        return self.m2


class EvolutionLibre(MenageTransform):
    def __init__(
        self,
        max_variation_revenus: int,
        max_variation_personnes_charge: int,
        max_variation_loyer: int,
    ) -> None:
        self.m1: Optional[Menage] = None
        self.m2: Optional[Menage] = None
        self.max_variation_revenus: int = max_variation_revenus
        self.max_variation_personnes_charge: int = max_variation_personnes_charge
        self.max_variation_loyer: int = max_variation_loyer

    def table_params_lines(self, s: Solver) -> List[List[Any]]:
        return [
            ["Variation maximale de revenus", self.max_variation_revenus],
            ["Variation maximale de personnes à charge", self.max_variation_personnes_charge],
            ["Variation maximale de loyer", self.max_variation_loyer]
        ]

    def transform_menage(self, m1: Menage, s: Solver) -> Menage:
        self.m1 = m1
        self.m2 = m1.evolution_libre(
            self.max_variation_revenus,
            self.max_variation_personnes_charge,
            self.max_variation_loyer,
            s
        )
        return self.m2


class DichotomicSearch(ABC):
    @abstractmethod
    def __init__(
        self,
        transform: MenageTransform,
        upper_limit: int,
        lower_limit: int
    ):
        self.transform_ = transform
        self.upper_limit_ = upper_limit
        self.lower_limit_ = lower_limit

    def transform(self) -> MenageTransform:
        return self.transform_

    @abstractmethod
    def table_params_lines(self, s: Solver) -> List[List[Any]]:
        pass

    @abstractmethod
    def search_constraint(
        self,
        s: Solver,
        rev1: Montant, rev2: Montant,
        current_search_param_value: int
    ) -> Any:
        pass

    @abstractmethod
    def new_search_param_value(self, s: Solver) -> float:
        pass


class TauxMarginal(DichotomicSearch):
    def __init__(
        self,
        augmentation: Augmentation,
        upper_limit: int,
        lower_limit: int
    ):
        self.transform_: Augmentation = augmentation

    def table_params_lines(self, s: Solver) -> List[List[Any]]:
        return []

    def search_constraint(
        self,
        s: Solver,
        rev1: Montant, rev2: Montant,
        current_search_param_value: int
    ) -> Any:
        self.deltai = rev2 - rev1
        augmentation_mensuelle_m = Montant.euros(self.transform_.augmentation_mensuelle, s)
        if current_search_param_value >= 0 and current_search_param_value <= 100:
            deltai_max = (augmentation_mensuelle_m * 12) % Taux.pourcent(100 - current_search_param_value)
        elif current_search_param_value >= 100:
            deltai_max = Montant.euros(0, s) - (augmentation_mensuelle_m * 12) % Taux.pourcent(current_search_param_value - 100)
        else:
            # If marginal rate is negative
            deltai_max = Montant.euros(0, s) - (augmentation_mensuelle_m * 12) % Taux.pourcent(- current_search_param_value)
        return self.deltai <= deltai_max

    def new_search_param_value(self, s: Solver) -> float:
        m = s.model()
        if self.deltai is None:
            raise TypeError("deltai is None !")
        deltai_m = self.deltai // 12
        deltai_m_print = deltai_m.evaluate(s)
        ratio: float = ((deltai_m_print.to_float() /  # type:ignore
            (self.transform_.augmentation_mensuelle))
            if self.transform_.augmentation_mensuelle != 0 else float("inf"))
        if ratio == float("inf"):
            marginal_tax_rate_found: float = float("inf")
        else:
            marginal_tax_rate_found = 100 - ratio * 100
        return marginal_tax_rate_found


class DifferenceRelative(DichotomicSearch):
    def __init__(
        self,
        transform: MenageTransform,
        max_true_min_false: bool,
        upper_limit: int,
        lower_limit: int
    ):
        self.transform_ = transform
        self.max_true_min_false = max_true_min_false
        self.upper_limit_ = upper_limit
        self.lower_limit_ = lower_limit

        self.deltai: Optional[Montant] = None

    def table_params_lines(self, s: Solver) -> List[List[Any]]:
        return [
            ["Pourcentage maximal autorisé", self.upper_limit_],
            ["Pourcentage minimal autorisé", self.lower_limit_]
        ]

    def search_constraint(
        self,
        s: Solver,
        rev1: Montant, rev2: Montant,
        current_search_param_value: int
    ) -> Any:
        self.deltai = rev2 - rev1
        if current_search_param_value >= 0:
            deltai_bound = self.transform_.menage_avant().revenu_salaires(s) % Taux.pourcent(current_search_param_value)
        else:
            deltai_bound = Montant.euros(0, s) - self.transform_.menage_avant().revenu_salaires(s) % Taux.pourcent(- current_search_param_value)
        if self.upper_limit_ >= 0:
            deltai_upper = self.transform_.menage_avant().revenu_salaires(s) % Taux.pourcent(self.upper_limit_)
        else:
            deltai_upper = Montant.euros(0, s) - self.transform_.menage_avant().revenu_salaires(s) % Taux.pourcent(- self.upper_limit_)
        if self.lower_limit_ >= 0:
            deltai_lower = self.transform_.menage_avant().revenu_salaires(s) % Taux.pourcent(self.lower_limit_)
        else:
            deltai_lower = Montant.euros(0, s) - self.transform_.menage_avant().revenu_salaires(s) % Taux.pourcent(- self.lower_limit_)
        if self.max_true_min_false:
            return And(self.deltai >= deltai_bound, self.deltai <= deltai_upper)
        else:
            return And(self.deltai <= deltai_bound, self.deltai >= deltai_lower)

    def new_search_param_value(self, s: Solver) -> float:
        m = s.model()
        if self.deltai is None:
            raise TypeError("deltai is None !")
        deltai_m = self.deltai
        deltai_m_print = deltai_m.evaluate(s)
        s1_m = self.transform_.menage_avant().revenu_salaires(s).evaluate(s)
        ratio: float = ((deltai_m_print.to_float() /  # type:ignore
            s1_m.to_float())
            if s1_m.to_float() != 0 else float("inf"))
        if ratio == float("inf"):
            relative_gain_found: float = float("inf")
        else:
            relative_gain_found = ratio * 100
        return relative_gain_found


class DifferenceAbsolue(DichotomicSearch):
    def __init__(
        self,
        transform: MenageTransform,
        max_true_min_false: bool,
        upper_limit: int,
        lower_limit: int
    ):
        self.transform_ = transform
        self.max_true_min_false = max_true_min_false
        self.upper_limit_ = upper_limit
        self.lower_limit_ = lower_limit

        self.deltai: Optional[Montant] = None

    def table_params_lines(self, s: Solver) -> List[List[Any]]:
        return [
            ["Valeur maximale autorisé", self.upper_limit_],
            ["Valeur minimale autorisé", self.lower_limit_]
        ]

    def search_constraint(
        self,
        s: Solver,
        rev1: Montant, rev2: Montant,
        current_search_param_value: int
    ) -> Any:
        self.deltai = rev2 - rev1
        deltai_bound = Montant.euros(current_search_param_value, s)
        deltai_upper = Montant.euros(self.upper_limit_, s)
        deltai_lower = Montant.euros(self.lower_limit_, s)
        if self.max_true_min_false:
            return And(self.deltai >= deltai_bound, self.deltai <= deltai_upper)
        else:
            return And(self.deltai <= deltai_bound, self.deltai >= deltai_lower)

    def new_search_param_value(self, s: Solver) -> float:
        m = s.model()
        if self.deltai is None:
            raise TypeError("deltai is None !")
        deltai_m = self.deltai
        deltai_m_print = deltai_m.evaluate(s)
        return deltai_m_print.to_float()  # type:ignore


class Question(ABC):
    @abstractmethod
    def __init__(
        self,
        menage_constraints: MenageConstraints,
        search: DichotomicSearch,
        initial_search_param_value: int,
        lower_bound: int,
        upper_bound: int,
        precision: int,
    ):
        self.menage_constraints_ = menage_constraints
        self.search_ = search
        self.initial_search_param_value_ = initial_search_param_value
        self.lower_bound_ = lower_bound
        self.upper_bound_ = upper_bound
        self.precision_ = precision

    def initial_search_param_value(self) -> int:
        return self.initial_search_param_value_

    def lower_bound(self) -> int:
        return self.lower_bound_

    def upper_bound(self) -> int:
        return self.upper_bound_

    def precision(self) -> int:
        return self.precision_

    def search(self) -> DichotomicSearch:
        return self.search_

    def menage_constraints(self) -> MenageConstraints:
        return self.menage_constraints_

    @abstractmethod
    def result_found_lines(self, s: Solver, new_search_param_value: float) -> List[Tuple[str, Any]]:
        pass

    @abstractmethod
    def before_request_message(self, current_search_param_value: int) -> str:
        pass

    @abstractmethod
    def intro_message(self) -> str:
        pass

    @abstractmethod
    def search_parameter_name(self) -> str:
        pass

    @abstractmethod
    def is_maximum(self) -> bool:
        pass


class TauxMarginalMaximalApresAugmentation(Question):
    def __init__(
        self,
        menage_constraints: MenageConstraints,
        augmentation_mensuelle: int,
        initial_search_param_value: int,
        lower_bound: int,
        upper_bound: int,
        precision: int
    ):
        augmentation = Augmentation(augmentation_mensuelle)
        self.search_: TauxMarginal = TauxMarginal(augmentation, upper_bound, lower_bound)
        self.menage_constraints_ = menage_constraints
        self.initial_search_param_value_ = initial_search_param_value
        self.lower_bound_ = lower_bound
        self.upper_bound_ = upper_bound
        self.precision_ = precision

    def result_found_lines(self, s: Solver, marginal_tax_rate_found: float) -> List[Tuple[str, Any]]:
        augmentation_mensuelle_m = Montant.euros(self.search_.transform_.augmentation_mensuelle, s)
        if self.search_.deltai is None:
            raise TypeError("deltai is None !")
        deltai_m = self.search_.deltai // 12
        return [
            ("Augmentation mensuelle des revenus", augmentation_mensuelle_m),
            ("Changement de revenu mensuel après redistribution", deltai_m),
            ("Taux marginal cumulé", "{0:.1f}%".format(marginal_tax_rate_found))
        ]

    def before_request_message(self, current_search_param_value: int) -> str:
        return "Existe-t-il un taux marginal au dessus de {}% dans ces conditions ?".format(current_search_param_value)

    def intro_message(self) -> str:
        return "Recherche d'un taux marginal anormalement élevé."

    def search_parameter_name(self) -> str:
        return "Taux marginal cumulé minimal"

    def is_maximum(self) -> bool:
        return True


class DifferenceMarriageOuPacs(Question):
    def __init__(self,
        menage_constraints: MenageConstraints,
        relative_true_absolue_false: bool,
        max_true_min_false: bool,
        initial_search_param_value: int,
        lower_bound: int,
        upper_bound: int,
        precision: int
    ):
        self.menage_constraints_ = menage_constraints
        self.relative_true_absolue_false = relative_true_absolue_false
        self.max_true_min_false = max_true_min_false
        self.initial_search_param_value_ = initial_search_param_value
        self.lower_bound_ = lower_bound
        self.upper_bound_ = upper_bound
        self.precision_ = precision

        transform = MarriageOuPacs()
        self.search_: Union[DifferenceRelative, DifferenceAbsolue]
        if self.relative_true_absolue_false:
            self.search_ = DifferenceRelative(transform, max_true_min_false, upper_bound, lower_bound)
        else:
            self.search_ = DifferenceAbsolue(transform, max_true_min_false, upper_bound, lower_bound)

    def result_found_lines(self, s: Solver, new_search_param_value: float) -> List[Tuple[str, Any]]:
        if self.search_.deltai is None:
            raise TypeError("deltai is None !")
        deltai_m = self.search_.deltai // 12
        return [
            ("Changement mensuel après marriage ou pacs", deltai_m),
            (("Différence relative avec marriage ou pacs", "{0:.1f}%".format(new_search_param_value))
            if self.relative_true_absolue_false else
            ("Différence absolue avec nouvel enfant", Montant.centimes(int(new_search_param_value * 100), s)))
        ]

    def before_request_message(self, current_search_param_value: int) -> str:
        return "Existe-t-il une différence {} de {} de {}{} dans ces conditions ?".format(
            "relative" if self.relative_true_absolue_false else "absolue",
            "plus" if self.max_true_min_false else "moins",
            current_search_param_value,
            "%" if self.relative_true_absolue_false else " €"
        )

    def intro_message(self) -> str:
        return "Recherche de la différence {} {} du revenu après distribution que peut induire un marriage ou pacs.".format(
            "relative" if self.relative_true_absolue_false else "absolue",
            "maximale" if self.max_true_min_false else "minimale",
        )

    def search_parameter_name(self) -> str:
        return "Différence {} {} avec le marriage ou pacs".format(
            "relative" if self.relative_true_absolue_false else "absolue",
            "minimale" if self.max_true_min_false else "maximale",
        )

    def is_maximum(self) -> bool:
        return self.max_true_min_false


class Difference2019IR2020(Question):
    def __init__(self,
        menage_constraints: MenageConstraints,
        relative_true_absolue_false: bool,
        max_true_min_false: bool,
        initial_search_param_value: int,
        lower_bound: int,
        upper_bound: int,
        precision: int
    ):
        self.menage_constraints_ = menage_constraints
        self.relative_true_absolue_false = relative_true_absolue_false
        self.max_true_min_false = max_true_min_false
        self.initial_search_param_value_ = initial_search_param_value
        self.lower_bound_ = lower_bound
        self.upper_bound_ = upper_bound
        self.precision_ = precision

        transform = Identite()
        self.search_: Union[DifferenceRelative, DifferenceAbsolue]
        if self.relative_true_absolue_false:
            self.search_ = DifferenceRelative(transform, max_true_min_false, upper_bound, lower_bound)
        else:
            self.search_ = DifferenceAbsolue(transform, max_true_min_false, upper_bound, lower_bound)

    def result_found_lines(self, s: Solver, new_search_param_value: float) -> List[Tuple[str, Any]]:
        if self.search_.deltai is None:
            raise TypeError("deltai is None !")
        deltai_m = self.search_.deltai // 12
        return [
            ("Changement mensuel après réforme PLF2020", deltai_m),
            (("Différence relative avec réforme PLF2020", "{0:.1f}%".format(new_search_param_value))
            if self.relative_true_absolue_false else
            ("Différence absolue réforme PLF2020", Montant.centimes(int(new_search_param_value * 100), s)))
        ]

    def before_request_message(self, current_search_param_value: int) -> str:
        return "Existe-t-il une différence {} de {} de {}{} dans ces conditions ?".format(
            "relative" if self.relative_true_absolue_false else "absolue",
            "plus" if self.max_true_min_false else "moins",
            current_search_param_value,
            "%" if self.relative_true_absolue_false else " €"
        )

    def intro_message(self) -> str:
        return "Recherche de la différence {} {} du revenu après distribution que peut induire la réforme PLF2020.".format(
            "relative" if self.relative_true_absolue_false else "absolue",
            "maximale" if self.max_true_min_false else "minimale",
        )

    def search_parameter_name(self) -> str:
        return "Différence {} {} avec la réforme PLF2020".format(
            "relative" if self.relative_true_absolue_false else "absolue",
            "minimale" if self.max_true_min_false else "maximale",
        )

    def is_maximum(self) -> bool:
        return self.max_true_min_false


class DifferenceNouvelEnfant(Question):
    def __init__(self,
        menage_constraints: MenageConstraints,
        relative_true_absolue_false: bool,
        max_true_min_false: bool,
        initial_search_param_value: int,
        lower_bound: int,
        upper_bound: int,
        precision: int,
        changement_logement: bool=False,
        nb_personnes_a_charge_init: Optional[int]=None,
        age_nouvel_enfant: Optional[int]=None,
    ):
        self.menage_constraints_ = menage_constraints
        self.relative_true_absolue_false = relative_true_absolue_false
        self.max_true_min_false = max_true_min_false
        self.initial_search_param_value_ = initial_search_param_value
        self.lower_bound_ = lower_bound
        self.upper_bound_ = upper_bound
        self.precision_ = precision

        transform = NouvelEnfant(
            menage_constraints,
            changement_logement,
            nb_personnes_a_charge_init,
            age_nouvel_enfant
        )

        self.search_: Union[DifferenceRelative, DifferenceAbsolue]
        if self.relative_true_absolue_false:
            self.search_ = DifferenceRelative(transform, max_true_min_false, upper_bound, lower_bound)
        else:
            self.search_ = DifferenceAbsolue(transform, max_true_min_false, upper_bound, lower_bound)

    def result_found_lines(self, s: Solver, new_search_param_value: float) -> List[Tuple[str, Any]]:
        if self.search_.deltai is None:
            raise TypeError("deltai is None !")
        deltai_m = self.search_.deltai // 12
        return [
            ("Changement mensuel après redistribution", deltai_m),
            (("Différence relative avec nouvel enfant", "{0:.1f}%".format(new_search_param_value))
            if self.relative_true_absolue_false else
            ("Différence absolue avec nouvel enfant", Montant.centimes(int(new_search_param_value * 100), s)))
        ]

    def before_request_message(self, current_search_param_value: int) -> str:
        return "Existe-t-il une différence {} de {} de {}{} dans ces conditions ?".format(
            "relative" if self.relative_true_absolue_false else "absolue",
            "plus" if self.max_true_min_false else "moins",
            current_search_param_value,
            "%" if self.relative_true_absolue_false else " €"
        )

    def intro_message(self) -> str:
        return "Recherche de la différence {} {} du revenu après distribution que peut induire un nouvel enfant.".format(
            "relative" if self.relative_true_absolue_false else "absolue",
            "maximale" if self.max_true_min_false else "minimale",
        )

    def search_parameter_name(self) -> str:
        return "Différence {} {} avec l'arrivée du nouvel enfant".format(
            "relative" if self.relative_true_absolue_false else "absolue",
            "minimale" if self.max_true_min_false else "maximale",
        )

    def is_maximum(self) -> bool:
        return self.max_true_min_false


class DifferenceChangementDistributionRevenus(Question):
    def __init__(self,
        menage_constraints: MenageConstraints,
        relative_true_absolue_false: bool,
        max_true_min_false: bool,
        initial_search_param_value: int,
        lower_bound: int,
        upper_bound: int,
        precision: int
    ):
        self.menage_constraints_ = menage_constraints
        self.relative_true_absolue_false = relative_true_absolue_false
        self.max_true_min_false = max_true_min_false
        self.initial_search_param_value_ = initial_search_param_value
        self.lower_bound_ = lower_bound
        self.upper_bound_ = upper_bound
        self.precision_ = precision

        transform = ChangementDistributionRevenus()

        self.search_: Union[DifferenceRelative, DifferenceAbsolue]
        if self.relative_true_absolue_false:
            self.search_ = DifferenceRelative(transform, max_true_min_false, upper_bound, lower_bound)
        else:
            self.search_ = DifferenceAbsolue(transform, max_true_min_false, upper_bound, lower_bound)

    def result_found_lines(self, s: Solver, new_search_param_value: float) -> List[Tuple[str, Any]]:
        if self.search_.deltai is None:
            raise TypeError("deltai is None !")
        deltai_m = self.search_.deltai // 12
        return [
            ("Changement mensuel après redistribution", deltai_m),
            (("Différence relative avec nouvelle distribution des revenus", "{0:.1f}%".format(new_search_param_value))
            if self.relative_true_absolue_false else
            ("Différence absolue avec nouvelle distribution des revenus", Montant.centimes(int(new_search_param_value * 100), s)))
        ]

    def before_request_message(self, current_search_param_value: int) -> str:
        return "Existe-t-il une différence {} de {} de {}{} dans ces conditions ?".format(
            "relative" if self.relative_true_absolue_false else "absolue",
            "plus" if self.max_true_min_false else "moins",
            current_search_param_value,
            "%" if self.relative_true_absolue_false else " €"
        )

    def intro_message(self) -> str:
        return "Recherche de la différence {} {} du revenu après distribution que peut induire une nouvelle distribution des revenus.".format(
            "relative" if self.relative_true_absolue_false else "absolue",
            "maximale" if self.max_true_min_false else "minimale",
        )

    def search_parameter_name(self) -> str:
        return "Différence {} {} avec une nouvelle distribution des revenus".format(
            "relative" if self.relative_true_absolue_false else "absolue",
            "minimale" if self.max_true_min_false else "maximale",
        )

    def is_maximum(self) -> bool:
        return self.max_true_min_false


class DifferenceAbsolueEvolutionLibre(Question):
    def __init__(
        self,
        menage_constraints: MenageConstraints,
        max_variation_revenus: int,
        max_variation_personnes_charge: int,
        max_variation_loyer: int,
        max_true_min_false: bool,
        initial_search_param_value: int,
        lower_bound: int,
        upper_bound: int,
        precision: int
    ) -> None:
        self.menage_constraints_ = menage_constraints
        self.max_variation_revenus_ = max_variation_revenus
        self.max_variation_personnes_charge_ = max_variation_personnes_charge
        self.max_variation_loyer_ = max_variation_loyer
        self.max_true_min_false_ = max_true_min_false
        self.initial_search_param_value_ = initial_search_param_value
        self.lower_bound_ = lower_bound
        self.upper_bound_ = upper_bound
        self.precision_ = precision

        transform = EvolutionLibre(
            self.max_variation_revenus_,
            self.max_variation_personnes_charge_,
            self.max_variation_loyer_,
        )
        self.search_: DifferenceAbsolue = DifferenceAbsolue(transform, max_true_min_false, upper_bound, lower_bound)

    def result_found_lines(self, s: Solver, new_search_param_value: float) -> List[Tuple[str, Any]]:
        if self.search_.deltai is None:
            raise TypeError("deltai is None !")
        deltai_m = self.search_.deltai // 12
        return [
            ("Changement mensuel après redistribution", deltai_m),
            ("Différence absolue après évolution", Montant.centimes(int(new_search_param_value * 100), s))
        ]

    def before_request_message(self, current_search_param_value: int) -> str:
        return "Existe-t-il une différence absolue de {} de {}€ dans ces conditions ?".format(
            "plus" if self.max_true_min_false_ else "moins",
            current_search_param_value,
        )

    def intro_message(self) -> str:
        return "Recherche de la différence absolue {} du revenu après distribution, après évolution libre.".format(
            "maximale" if self.max_true_min_false_ else "minimale",
        )

    def search_parameter_name(self) -> str:
        return "Différence absolue {} après évolution libre".format(
            "minimale" if self.max_true_min_false_ else "maximale",
        )

    def is_maximum(self) -> bool:
        return self.max_true_min_false_
