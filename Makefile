check: *.py
	mypy main.py

run: check
	python main.py

hypotheses:
	grep -r "HYPOTHÈSE:" ./ | sed -e 's/^.*\.py\:\s*\#\s//'
