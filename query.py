# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from z3 import *
from montant import *
from foyerfiscal import *
from IR2019 import *
from IR2020 import *
from solveur import *
from individu import *
from menage import *
from prime_activite2019 import *
from allocations_familiales2019 import *
from APL2019 import *
import time
from typing import Set, Optional, List, Tuple, Union, Any
from verifisc_telegram_bot import post_message, post_file
from random_word import RandomWords  # type: ignore
from questions import *
import subprocess
from tabulate import tabulate


def print_message(n: str, msg: str, send_to_telegram: bool) -> None:
    with open("results/" + n + ".txt", "a") as f:
        f.write(msg + "\n")
    if send_to_telegram:
        post_message(msg)
    else:
        print(msg)


def dichotomic_search(
    question: Question,
    timeout: Optional[int]=None,
    send_to_telegram: bool=False,
    features: Set[str]=set(),
    version_label: str=subprocess.check_output(["git", "describe", "--always"]).strip()  # type: ignore
) -> None:
    current_search_param_value: int = question.initial_search_param_value()
    lower_bound: int = question.lower_bound()
    upper_bound: int = question.upper_bound()
    # For a maximum search, the lower bound will correspond to a possible value,
    # the upper bound to an impossible value.
    # It is the converse for a minimum search.
    if current_search_param_value < lower_bound or (
        current_search_param_value > upper_bound
    ):
        raise TypeError("wrong parameters !")
    filename: Optional[str] = None
    found_marginal_rate: Optional[int] = None

    s = Solver()

    n = RandomWords().get_random_word(includePartOfSpeech="noun").upper()
    print_message(n, question.intro_message(), send_to_telegram)
    print_message(n, "Nom de la recherche: {}".format(n), send_to_telegram)
    table_params_lines: List[List[Any]] = (question.menage_constraints().table_params_lines(s) +
        question.search_.transform_.table_params_lines(s) +
        question.search_.table_params_lines(s) + [
        ["Dispositions prises en compte", ', '.join(features)]
    ])

    with open("results/" + n + ".txt", "a") as f:
        f.write("\nTableau des paramètres de la recherche:\n" + str(
            tabulate(table_params_lines,
                ["Paramètre", "Valeur"],
                tablefmt="pipe"
            )
        ) + "\n")
    if send_to_telegram:
        post_file("results/" + n + ".txt")

    if not (timeout is None):
        s.set("timeout", 1000 * timeout)

    m1 = Menage.variable("m1", s)
    question.menage_constraints().constrain_menage(m1, s)
    s1 = m1.revenu_salaires(s)

    # HYPOTHÈSE: les revenus proviennent uniquement de salaires
    m2 = question.search_.transform_.transform_menage(m1, s)
    question.menage_constraints().constrain_menage(m2, s)
    s2 = m2.revenu_salaires(s)

    comparison_lines: List[Tuple[str, Montant, Montant]] = [
        ("Salaire annuel", s1, s2)
    ]

    bir2019_1 = IRMenage2019(m1)
    bir2019_2 = IRMenage2019(m2)
    bir2020_1 = IRMenage2020(m1)
    bir2020_2 = IRMenage2020(m2)
    bam2019_1 = AllocationsFamiliales2019(m1)
    bam2019_2 = AllocationsFamiliales2019(m2)
    bapl2019_1 = APL2019(m1)
    bapl2019_2 = APL2019(m2)
    bpa2019_1 = PrimeActivite2019(bam2019_1, bapl2019_1)
    bpa2019_2 = PrimeActivite2019(bam2019_2, bapl2019_2)
    rev1 = s1
    rev2 = s2
    if "IR2019" in features:
        [rfr1, droits_simples1, decote1, rscr1, ir1] = bir2019_1.calcul_avec_etapes(s)
        [rfr2, droits_simples2, decote2, rscr2, ir2] = bir2019_2.calcul_avec_etapes(s)
        rev1 -= ir1
        rev2 -= ir2
        comparison_lines += [
            ("[2019] Revenu fiscal de référence", rfr1, rfr2),
            ("[2019] Droits simples", droits_simples1, droits_simples2),
            ("[2019] Décote", decote1, decote2),
            ("[2019] RSCR", rscr1, rscr2),
            ("[2019] IR", ir1, ir2),
        ]

    if "IR2020" in features:
        [rfr1, droits_simples1, decote1, rscr1, ir1] = bir2020_1.calcul_avec_etapes(s)
        [rfr2, droits_simples2, decote2, rscr2, ir2] = bir2020_2.calcul_avec_etapes(s)
        rev1 -= ir1
        rev2 -= ir2
        comparison_lines += [
            ("[2020] Revenu fiscal de référence", rfr1, rfr2),
            ("[2020] Droits simples", droits_simples1, droits_simples2),
            ("[2020] Décote", decote1, decote2),
            ("[2020] RSCR", rscr1, rscr2),
            ("[2020] IR", ir1, ir2),
        ]

    if "2019IR2020" in features:
        [rfr1, droits_simples1, decote1, rscr1, ir1] = bir2019_1.calcul_avec_etapes(s)
        [rfr2, droits_simples2, decote2, ir2] = bir2020_2.calcul_avec_etapes(s)
        rev1 -= ir1
        rev2 -= ir2
        comparison_lines += [
            ("[2019-2020] Revenu fiscal de référence", rfr1, rfr2),
            ("[2019-2020] Droits simples", droits_simples1, droits_simples2),
            ("[2019-2020] Décote", decote1, decote2),
            ("[2019-2020] RSCR", rscr1, Montant.euros(0, s)),
            ("[2019-2020] IR", ir1, ir2),
        ]
    # HYPOTHÈSE: Pas de non-recours aux allocations
    if "AF" in features:
        [am1base, am1majoration, am1cf, am1paje, am1] = bam2019_1.calcul_avec_etapes(s)
        [am2base, am2majoration, am2cf, am2paje, am2] = bam2019_2.calcul_avec_etapes(s)
        rev1 += am1 * 12
        rev2 += am2 * 12
        comparison_lines += [
            ("Base allocations", am1base, am2base),
            ("Majoration allocations familiales", am1majoration, am2majoration),
            ("Complément familial", am1cf, am2cf),
            ("PAJE", am1paje, am2paje),
            ("Allocations familiales", am1, am2),
        ]
    if "ARS" in features:
        am1rs = bam2019_1.allocation_rentree_scolaire(s)
        am2rs = bam2019_2.allocation_rentree_scolaire(s)
        rev1 += am1rs
        rev2 += am2rs
        comparison_lines += [
            ("Allocation rentrée scolaire", am1rs, am2rs),
        ]
    if "BC" in features:
        am1bc = bam2019_1.bourses_college_trimestrielles(s)
        am2bc = bam2019_2.bourses_college_trimestrielles(s)
        rev1 += am1bc * 3
        rev2 += am2bc * 3
        comparison_lines += [
            ("Bourses collège", am1bc, am2bc),
        ]
    if "BL" in features:
        am1bl = bam2019_1.bourses_lycee_annuelles(s)
        am2bl = bam2019_2.bourses_lycee_annuelles(s)
        rev1 += am1bl
        rev2 += am2bl
        comparison_lines += [
            ("Bourses lycée", am1bl, am2bl),
        ]
    if "APL" in features:
        apl1 = bapl2019_1.montant(s)
        apl2 = bapl2019_2.montant(s)
        rev1 += apl1 * 12
        rev2 += apl2 * 12
        comparison_lines += [
            ("APL", apl1, apl2),
        ]
    if "PA" in features:
        if not ("APL" in features and "AF" in features):
            raise TypeError("La prime d'activité dépend des allocations familiales et de l'APL!")
        else:
            pa1 = bpa2019_1.montant(s)
            pa2 = bpa2019_2.montant(s)
            rev1 += pa1 * 12
            rev2 += pa2 * 12
            comparison_lines += [
                ("Prime d'activité", pa1, pa2),
            ]

    found_search_param_value = None
    filename = None
    while (upper_bound - lower_bound >= question.precision()):
        print_message(n, "[{}] ".format(n) + question.before_request_message(current_search_param_value), send_to_telegram)
        s.push()
        t1 = time.time()
        c = s.check(question.search_.search_constraint(
            s,
            rev1,
            rev2,
            current_search_param_value
        ))
        t2 = time.time()
        lines: List[Tuple[str, Any]] = []

        comparison_lines_print = comparison_lines + [
            ("Net touché", rev1, rev2),
        ]

        if c == sat:
            new_search_param_value = question.search_.new_search_param_value(s)
            lines += question.result_found_lines(s, new_search_param_value)
        filename_instance = print_check_results_menage(c, s, m1, m2, lines, comparison_lines_print,
            question.intro_message() + "\n" +
            "Voici la valeur des paramètres de la requête :\n\n{}\n\n".format(str(tabulate(
                [[question.search_parameter_name(), "{}".format(current_search_param_value)]] + table_params_lines,
                ["Paramètre", "Valeur"],
                tablefmt="pipe"
            ))) +
            "Résultat trouvé en {:.2f} secondes avec {} Mo de mémoire RAM consommée.".format(t2 - t1, s.statistics().get_key_value('max memory')),
            send_to_telegram,
            version_label
        )

        # Here we have finished!
        res: Optional[Tuple[float, str]]
        if c == sat:
            res = new_search_param_value, filename_instance
        else:
            res = None

        if res is None:
            print_message(
                n,
                "[{}] Impossible d'avoir une valeur au dessus de {}.".format(n, current_search_param_value),
                send_to_telegram
            )
            if question.is_maximum():
                upper_bound = current_search_param_value
            else:
                lower_bound = current_search_param_value
            current_search_param_value = (upper_bound + lower_bound) // 2
        else:
            found_search_param_value_float, filename = res
            if found_search_param_value_float == float("inf"):
                print_message(
                    n,
                    "[{}] Trouvé une valeur infinie : voir fichier {}. Arrêt de la recherche".format(n, filename),
                    send_to_telegram
                )
                return
            found_search_param_value = int(found_search_param_value_float)
            print_message(
                n,
                "[{}] Trouvé une valeur de {} : voir fichier {}".format(n, found_search_param_value, filename),
                send_to_telegram
            )
            if question.is_maximum():
                if found_search_param_value == lower_bound:
                    lower_bound += question.precision()
                else:
                    lower_bound = found_search_param_value
            else:
                if found_search_param_value == upper_bound:
                    upper_bound -= question.precision()
                else:
                    upper_bound = found_search_param_value
            current_search_param_value = (lower_bound + upper_bound) // 2
            s.pop()

    print_message(n, "[{}] La recherche est terminée.".format(n), send_to_telegram)
    if found_search_param_value is None:
        print_message(n, "[{}] Aucun résultat significatif n'a été trouvé.".format(n), send_to_telegram)
    else:
        print_message(n, "[{}] Le résultat est de {}, et correspond ".format(n, found_search_param_value) +
        "à la situation du fichier {}".format(filename),
        send_to_telegram)
