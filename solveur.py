# This file is part of the program Verifisc whose purpose is to model a part of the
# French socio-fiscal system and craft Z3 queries aimed at analysing it.
#
# Copyright (C) 2019 Inria, contributor: Denis Merigoux <denis.merigoux@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from z3 import *
from montant import *
from foyerfiscal import *
from menage import *
from individu import *
from typing import List, Tuple, Any
import names  # type: ignore
import datetime
from verifisc_telegram_bot import post_file
from os import mkdir
import subprocess
from tabulate import tabulate


def print_check_results_menage(
    c: Any,
    s: Solver,
    m_avant: Menage,
    m_apres: Menage,
    vars: List[Tuple[str, Any]],
    compared_vars: List[Tuple[str, Any, Any]],
    descr: str,
    post_to_telegram: bool,
    label: str
) -> str:
    # Returns the name of the result file
    name = names.get_full_name()
    filename = "results/" + name.replace(" ", "_") + ".txt"
    print("Writing results to {}\n".format(filename))
    file = open(filename, "w")

    file.write("Fichier généré par le prototype Python de Verifisc (version {}) le {}\n\n".format(
        label,
        datetime.datetime.now().isoformat()
    ))
    print(s.statistics())

    file.write(descr + "\n\n")

    if c == sat:
        file.write("Possible ! Voici un ménage remplissant les conditions :\n\n")
        m_avant_print = m_avant.evaluate(s)
        file.write(str(m_avant_print))
        file.write("\n\nLe même ménage, après le changement :\n\n")
        m_apres_print = m_apres.evaluate(s)
        file.write(str(m_apres_print))
        file.write("\n\nMaintenant, les montants calculés avant et après:\n\n")
        lines_comp = []
        for name_var, var1, var2 in compared_vars:
            try:
                var3 = var2 - var1
                var1_print = var1.evaluate(s)
                var2_print = var2.evaluate(s)
                var3_print = var3.evaluate(s)
                line = [name_var, var1_print, var2_print, var3_print]
            except AttributeError:
                line = [name_var, var1, var2, var3]
            lines_comp.append(line)
        table_compare = tabulate(
            lines_comp,
            ["Montant", "Valeur avant", "Valeur après", "Delta"],
            tablefmt="pipe"
        )
        file.write(str(table_compare))
        file.write("\n\nEt voici les autres variables demandées :\n\n")
        lines = []
        for name_var, var in vars:
            try:
                var_print = var.evaluate(s)
                line = [name_var, var_print]
            except AttributeError:
                line = [name_var, var]
            lines.append(line)
        table = tabulate(
            lines,
            ["Caractéristique", "Valeur"],
            tablefmt="pipe"
        )
        file.write(str(table))

    elif c == unsat:
        file.write("Impossible !")
    elif c == unknown:
        file.write("Le prouveur ne trouve rien...")
        file.write(s.reason_unknown())
    else:
        file.write(c)

    file.close()

    if post_to_telegram and c == sat:
        post_file(filename)

    return name.replace(" ", "_") + ".txt"  # type:ignore
